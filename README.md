# karp-theme / karpador
"Karpador" is a [Zola](https://github.com/getzola/zola) made with Bulma,
initially based on the zola port of hyde.

If I manage to do this, it should be a simple and clean theme; its primary
target is providing a nice look for my own blog, so I don't know how
well-suited it will be for other use cases. Have fun!

## Theme specifics

This theme uses a very opinionated structure; two sections are important:

* pages: Meant for static pages that don't change often. Pages are listed in the
  main menu of the page unless they have `extra.visible` set to `false`.
* articles: Those are meant to be blog content that changes (gets edited/added)
  more often. They are listed on the home page of the Zola site.

If you've used Pelican before, these sections might sound familiar to you;
Pelican sites structure content the same way, with different directories for
articles and pages. This is indeed heavily inspired by the way Pelican does it,
but with a bit less flexibility because I want to keep things simple.

### extra settings

* `karp_logo` (default `""`) - The logo of the site, to be displayed in the
  navigation menu. Ignored if set to an empty string or unset; you should use
  either `karp_logo` or set set `karp_showtitle` to `true` to get the intended
  design.
* `karp_showtitle` (default `false`) - If true, the title of the site is shown
  in the main navigation menu.
* `karp_links` (default `[]`) - A list of links to be displayed as buttons in
  the main navigation menu. Items have to be in the following format:
  ```{ url = "http://the-link-URL.xyz", name = "The display name", color = "The button color" }```
  For valid color values, look [here](https://bulma.io/documentation/elements/button/#colors).
  The default is `is-dark`.
* `karp_links_separate` (default `true`) - If set to true, a second button list
  is created for the links, separating them from the page links.
* `karp_sidebar_end` - A string that'll be put in centered at the end of the
  sidebar, e.g. for a copyright notice or a license.

### Taxonomies

This theme supports only one taxonomy, and it has to be called `tags` - and it
is required for the theme to work. There are no categories because I don't need
them, and no authors because I am the only author of my blog.

Articles are meant to be tagged, whereas pages *can* be tagged, but I don't
know why anyone would want that.

## Screenshots

The home page (article list):

![Home page (article list)](./screenshot.png)

An article:

![Article](./screenshot2.png)

And a page:

![Page](./screenshot3.png)
