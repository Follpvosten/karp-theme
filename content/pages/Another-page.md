+++
title = "Another page"
date = 2018-03-07
weight = 2

[extra]
visible = true
color = "is-dark"
hide_date = true
+++

Just another page to showcase the ordering in the menu.
(`weight = 2` here lists the page as the second one)
