+++
title = "Pages"
date = 2018-02-23

weight = 1

[extra]
visible = true
color = "is-dark"
hide_date = true
+++

The term "pages" was inherited from Pelican here.

Pages in the "pages" section are meant to be static and don't change often;
they are linked in the navigation column of the site unless `page.extra.visible`
is set to `false`. The color of the link is set by `page.extra.color`, and the
date can be hidden (it normally is) by setting `page.extra.hide_date` to `true`.

The order of the pages in the menu can be changed by changing the mandatory
weight field of the page.

The opposite are "articles", which show up in the list on the home page and
will be added or changed more often.
